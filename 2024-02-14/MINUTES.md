## Meeting details

Held on 13/12/2023 at 15:00 by Zoom.

## Participants

- MAX IV
  - Benjamin Bertrand
  - Johan Forsberg
  - Hanno Perrey
- ESRF
  - Guillaume Communie
- SOLARIS
  - Wojciech Wantuch
- SOLEIL
  - Raphael Girardot
- ALBA
  - Emilio Morales
  - Miquel Navarro
  - Zbigniew Reszela
- ESO
  - Arturo Hoffstadt
- Others
  - Juan de la Figuera

## Minutes

### Taurus Performance Optimization
- M.Navarro explains M.Caixal leaves ALBA but TPO is still an annual objective for ALBA Controls section, so we will continue investing time.

### Taurus Recent Issues - Round Table
- E.Morales talks about the latest issues created in Taurus:
  - [#1319](https://gitlab.com/taurus-org/taurus/-/issues/1319): User probably is facing problems with PyQt5 version.
  - [#1320](https://gitlab.com/taurus-org/taurus/-/issues/1320): Known issue. E.Morales refers the user to Taurus wiki.
  - [#1321](https://gitlab.com/taurus-org/taurus/-/issues/1321): Need time to investigate this issue.
  - [#1315](https://gitlab.com/taurus-org/taurus/-/issues/1315): J.Forsberg work on this. MR draft in taurs-pyqtgraph project.
  - [#1322](https://gitlab.com/taurus-org/taurus/-/issues/1322): M.Navarro is working on it.
  - [#1323](https://gitlab.com/taurus-org/taurus/-/issues/1323): Need deeper invetigation. M.Navarro will check it.
  - [#1299](https://gitlab.com/taurus-org/taurus/-/issues/1299): A.Hoffstadt talks about the progress to PyQt6/PySide6.
  - [#1318](https://gitlab.com/taurus-org/taurus/-/issues/1318): Commented with H.Perrey his foundings. He will perform some checks at Taurus core level.

### Ongoing Developments & MR Status
- M.Navarro explains about:
  - Double Tango DB merge requests [!1270](https://gitlab.com/taurus-org/taurus/-/merge_requests/1270). Review is in progress.
  - Explained that J.Ramos was working on a taurus-pyqtgraph feature, see [!112](https://gitlab.com/taurus-org/taurus_pyqtgraph/-/merge_requests/112).
  - J.Ramos will check MAXIV MR [!113](https://gitlab.com/taurus-org/taurus_pyqtgraph/-/merge_requests/113).


### New Releases
- M.Navarro explained that we plan to release a new minor version. This must be an official relase. He asks for volunteers to perform manual tests.

### Others
- M.Navarro informs that ALBA sent a list of externalization options that N.Leclerc requested from Tango Steering Comittee.
- A.Hoffstad asks for colaboration between Max Plank institute and ESO.
- Z.Reszela informs about his new responsabilities in ALBA.
- E.Morales suggest next meeting on 13.03.2024.
