# Taurus Follow up meeting

## Agenda

1. Taurus Performance Optimization.
2. Taurus recent issues - round table.
3. Taurus ongoing developments & merge request status.
 - Add support for PyQt6/PySide6 #1299
  - Progress made on taurus.external.qt that adds support for PyQt6 and PySide6.
  - All tests for taurus.external.qt passes.
  - New docker images were created to test this.
  - Currently working on: enumeration namespace differences.
4. New releases.
5. Others.