## Meeting details

Held on 05/06/2024 at 15:00 by Zoom.

## Participants

- ESO
    - Arturo Hoffstad
    - Marzena Kaczmarczyk

- SOLEIL
    - Patrik Madela
    - Raphael Girardot
    - Ludmila Klenov

- MAXIV
    - Benjamin Bertrand

- ALBA
    - Jose Ramos
    - Miquel Navarro
    - Emilio Morales

## Minutes

### Taurus Performance Optimization
- M.Navarro explains that he continue working in the project. We detected the origin of the problem and he is working to solve it.

### Taurus Recent Issues - Round Table
- E.Morales and J.Ramos talks about the latest issues created. Regarding taurus:
    - #1341: matplotlib support for graphs/trends.
    - #1342: DevEnum support.
    - #1343: TaurusMainWindow smart save settings.
    - #1344: Error saving configurations.
    - #1345: Widgets hard to read on dark color scheme.
    - #1346: Scientific notation not available through setFormatter in taurus 4.4.
    - #1347: TaurusDevicePanel uses Factory from default scheme instead of the models.
    - #1348: Drop guiqwt dependency?
    - #1349: Documentation mistake.
- Regarding taurus-pyqtgraph:
    - #125: Problems with Statistics.
    - #126: Taurus plot view range not properly saved in setting.
    - #127: Crash linked to TaurusTrend widget: wrapped C/C++ object of type AxisItem has been deleted.
- P.Madela asked about issues posted by L.Klenov. We explain that the issues are in the queue, to be solved and if they prefer, they can submit a merge request with a solution proposal. Due to the high demand for merge request reviews and submitted issues, currently we give more priorities to merge requests. Help with issues is slower then we would like it to be, sorry about that.

### Ongoing Developments & MR Status
- E.Morales explains about taurus:
    - !1273:  Validation of dot-ended URL. Ready to merge.
    - !1275: Allow to set qt properties on PanelDescription. Merged.
    - !1279: Draft: TaurusLabel.valueChangedSignal not emitted. Ready to merge.
    - !1280: #1347 TaurusDevicePanel wrong factory fix. Merged.
    - !1281: Load ui files with taurus gui command. Pipeline enhancement.

### About Taurus Workshop
- M.Navarro sumarize taurus workshop held during past Tango meeting. All materials used during the workshop are available at taurus-training repository. A.Hoffstad said that he will contribute to documentation/training. He will create a merge request.

### Others
- A.Hoffstad explains that he is working on a paper and poster for a conference that explains the taurus plugins developed at ESO.
- Open discussion about code formatters (black, ruf) and code linters (flake8). A.Hoffstad and B.Bertrand suggest to start using mypy too.
- B.Bertrand offered to improve taurus pipelines with a new pre-commit stage. He will propose some configuration changes.
- Next meeting will be held on **03.07.2024**.