## Meeting details

Held on 06/11/2024 at 15:00 via Zoom.

## Participants

- Benjamin Bertrand (MAX-IV)
- Maciej Mleczko (SOLARIS)
- Wojciech Wantuch (SOLARIS)
- Patrick Madela (SOLEIL)
- Raphaël Girardot (SOLEIL)
- Miquel Navarro (ALBA)
- Jose Ramos (ALBA)
- Oriol Vallcorba (ALBA)  

## Minutes

### Taurus 5.2 Release

Special thanks to everyone involved in manual testing and providing valuable feedback. This has helped improve the testing guide and detect several related bugs.

ALBA reports its experience deploying Taurus 5.2 in the control room and four beamlines (in progress):
- Some GUIs where using `TaurusAttribute` values read from devices during the starting of the application. Now, in this situation, the read is done with `cache=True` and return `None`, not blocking to wait for the polling cycle/event to fill the cache. These GUIs were adapted to use `cache=False` to get real-time reads.
- When models after the applicaton has started (e.g., via tab change or new form), each attribute takes 3 seconds as it waits for a polling cycle. The current solution is to set `TANGO_FORCE_READ_IF_SUBSCRIPTION_FAILS` to "ALWAYS," forcing reads with cache=False (previous taurus behaviour). We are evaluating other solutions to benefit from TPO improvements also in this case.
- Disabling the events using taurus delayed subscriber methods was not working as it was done in some GUIs. Now a proper option to set it is being prepared ([!1326](https://gitlab.com/taurus-org/taurus/-/merge_requests/1326))

SOLEIL:
- Asked if Panic is affected by these issues. First tests show that it is not affected but tests are needed. Same applies for Sardana, is seems ok, but will be monitored during normal beamline operation.
- Also need to enable manually the polling in some attributes running in old Tango versions where events do not work. They are concerned about point 3 mentioned before. They are interested in the API addition to disable event subscriptions and rely only on polling.
- Deployment in the coming weeks; currently testing in a staging environment.

MAX-IV: 
- Deployment scheduled on one beamline next week.

### TPO (Taurus Performance Optimization)

The 5.2 release includes most of optimizations so far, affecting mainly attributes without events (polling). Now, for the attributes with events a request to tango has been done to avoid the read inside the subscribe or to do it asynchronously (https://gitlab.com/tango-controls/cppTango/-/issues/1344)

During the TPO another idea to improve the management of attributes raising timeouts and causing issues is to have a secondary polling thread for "unhealthy" attributes. A proposal is made as [TEP22](https://gitlab.com/taurus-org/taurus/-/merge_requests/1313).

### Recent Issues - Round Table
Fast review of the open taurus issues since last follow-up:
- [#1369](https://gitlab.com/taurus-org/taurus/-/issues/1369) TaurusImage: Y cross-section not aligned with top border of the image -> guiqwt?
- [#1370](https://gitlab.com/taurus-org/taurus/-/issues/1370) TaurusGui: Once a docking widget is floating, it cannot be put back in the MainWindow -> Seems to be related with Wayland but it happens in Fedora 38 and 40 and not on Ubuntu.
- [#1371](https://gitlab.com/taurus-org/taurus/-/issues/1371) TaurusForm: editing while in compact mode forcefully returns the gui to read/write mode -> Still discussion about it and narrowing the cause
- [#1372](https://gitlab.com/taurus-org/taurus/-/issues/1372) taurus demo: buttons not working on Message Panel & Input Dialog -> Small issue during tests.
- [#1374](https://gitlab.com/taurus-org/taurus/-/issues/1374) Taurus : Incompatibility with Tango database encoding -> problem with “micro” symbol.
- [#1376](https://gitlab.com/taurus-org/taurus/-/issues/1376) Writing a value out of range prevents other attributes from writing -> With the apply button
- [#1377](https://gitlab.com/taurus-org/taurus/-/issues/1377) Switch widgets images and trend2d to logarithmic scale  -> guiqwt?
- [#1379](https://gitlab.com/taurus-org/taurus/-/issues/1379) Taurus gui exception pop-up dialog repeated every polling period for unexisting attributes. -> MR created (!1323)
- [#1383](https://gitlab.com/taurus-org/taurus/-/issues/1383) Question related to widget testing and tango
- [#1384](https://gitlab.com/taurus-org/taurus/-/issues/1384) QtDesigner is logging DEBUG by default -> proposal to fix. MR pending.
- [#1385](https://gitlab.com/taurus-org/taurus/-/issues/1385) designMode is not injected to components generated from ui files
- [#1386](https://gitlab.com/taurus-org/taurus/-/issues/1386) Issue_attach is called when setModel is called, even in designMode
- [#1387](https://gitlab.com/taurus-org/taurus/-/issues/1387) Problem to access state from another tango DB  -> may be a regression from multiple tangoDB support
- [#1388](https://gitlab.com/taurus-org/taurus/-/issues/1388) taurus form: PY_SSIZE_T_CLEAN macro must be defined for '#' formats  -> Issue fixed in PyQt versions not compatible with Taurus designer. Patch proposed.
- [#1389](https://gitlab.com/taurus-org/taurus/-/issues/1389) taurus form --verbose unsupported -> bug with the (deprecated) guiqwt subcommand

Some were created and already closed:
- [#1368](https://gitlab.com/taurus-org/taurus/-/issues/1368) taurus designer do not expose taurus widgets -> Known issue with workaround 
- [#1373](https://gitlab.com/taurus-org/taurus/-/issues/1373) Drag and drop a model doesn't add it to model chooser list -> Reverted MR causing it.
- [#1375](https://gitlab.com/taurus-org/taurus/-/issues/1375) IssueTango formatting is not applied to device attribute values -> Solved using --default-formatter
- [#1378](https://gitlab.com/taurus-org/taurus/-/issues/1378) Cannot add model from "advanced settings" new panel wizard -> Reverted MR causing it.
- [#1380](https://gitlab.com/taurus-org/taurus/-/issues/1380) PYQTDESIGNERPATH is overwritten -> MR !1324
- [#1381](https://gitlab.com/taurus-org/taurus/-/issues/1381) AttributeError: 'QWheelEvent' object has no attribute 'delta'  -> Already fixed in current version
- [#1382](https://gitlab.com/taurus-org/taurus/-/issues/1382) Question: Resource file like logos:taurus.png  -> Found the way

And one in taurus_pyqtgraph:
- [#131](https://gitlab.com/taurus-org/taurus_pyqtgraph/-/issues/131) Infinite lines are not supported -> Workaround proposed, but we need to officially support them

### MR status and ongoing developments
- [!1318](https://gitlab.com/taurus-org/taurus/-/merge_requests/1318) Increase min numpy version to 1.17 -> Debian 10 package is 1.16.
- [!1319](https://gitlab.com/taurus-org/taurus/-/merge_requests/1319) Close TaurusApplication on ctrl+c (SIGINT signal) -> Anything against this?
- [!1322](https://gitlab.com/taurus-org/taurus/-/merge_requests/1322) Use TaurusModelSelector in TaurusForm (2) -> Reverted from release. Now with fixes for detected issues. Reviewers?
- [!1323](https://gitlab.com/taurus-org/taurus/-/merge_requests/1323) Prevent TaurusValue to return a non-list result as default read widget and to destroy an unexisting widget.  -> Issue detected in tests. Reviewers?
- [!1325](https://gitlab.com/taurus-org/taurus/-/merge_requests/1325) Remove tests on Debian 9 stretch and add tests on Debian 12 bookworm. -> No more Debian 9 packages since 5.1.8. Ready to merge.
- [!1326](https://gitlab.com/taurus-org/taurus/-/merge_requests/1326) Add option to disable tango events  -> Ready to review. Reviewers: Oriol, ..?
- [!1327](https://gitlab.com/taurus-org/taurus/-/merge_requests/1327) jdraw: Fixed int for QColor args -> To open old (PyQt4) jdw files. Ready to review.
- [!1328](https://gitlab.com/taurus-org/taurus/-/merge_requests/1328) taurusgraphic: Close thread in the end -> Ready to review.
- [!1329](https://gitlab.com/taurus-org/taurus/-/merge_requests/1329) jdraw: Enable antialiasing -> Set it as default which sounds reasonable. Ready to merge.
- [!1324](https://gitlab.com/taurus-org/taurus/-/merge_requests/1324)  Prepend taurus path to PYQTDESIGNERPATH (already merged)

### Increase minimum Python version support from 3.5 to 3.7

Since ALBA no longer packages for Debian 9, minimum Python version compatibility will be increased to 3.7 (now limited by the Debian 10 system version). This will be implemented in the next minor release. Everybody agrees.

### Next Releases

We aim to release more frequently. Specially the "patch" releases that will not require an exhaustive testing, only test the added fixes/features.

The next release will be 5.2.1 that should include for sure:
- Fix for the reverted MR for the release: [!1322](https://gitlab.com/taurus-org/taurus/-/merge_requests/1322) Use TaurusModelSelector in TaurusForm (2)
- Fix to allow disabling tango events: [!1326](https://gitlab.com/taurus-org/taurus/-/merge_requests/1326) Add option to disable tango events
and other additions and fixes detected recently and/or urgent requests.

Then, the following minor release 5.3, is planned to include [support for Qt6 (!1300)](https://gitlab.com/taurus-org/taurus/-/merge_requests/1300) and to increase minimum python version to 3.7.

We can use the milestones of GitLab or label issues/MRs as "next-patch-release"

### AOB
- Consider migrating from `guiqwt` as it is causing several issues and it is not mantained anymore. Plan the migration to pyqtgraph.
- Next meeting is scheduled for **11.12.2024**.