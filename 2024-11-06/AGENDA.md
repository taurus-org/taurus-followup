# Taurus Follow up meeting

Meeting on 06.11.2024 at 15h

## Agenda

1. Taurus 5.2 Release.
2. TPO
3. Recent issues - round table.
4. Merge request status - ongoing developments
5. Increase minimum Python version support from 3.5 to 3.7
6. Next Releases
   - 5.2.1 -> fixes and small additions (taurus form,...)
   - 5.3 -> qt6 and py3.7
7. AOB
