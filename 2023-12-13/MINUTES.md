## Meeting details

Held on 13/12/2023 at 15:00 by Zoom.

## Participants

- MAX IV
  - Benjamin Bertrand
- S2 Innovation
  - Michał Gandor
- ESRF
  - Guillaume Communie
  - Nicolas Leclercq
- SOLARIS
  - Krzysztof Madura
  - Wojciech Wantuch
- SOLEIL
  - Raphael Girardot
- ALBA
  - Martí Caixal
  - Emilio Morales
  - Miquel Navarro
  - Jose Ramos
- Others
  - Juan de la Figuera

## Minutes

### Taurus Performance Optimization
- M.Caixal explains why we need FQDN functions. Problems are related to events with old Tango versions. The community is interested in knowing which versions are compatible and which ones are not.
- G.Communie commented on the problem with polling. He is facing issues with it and a GUI he is developing. He asks about how to reproduce issue [#1278](https://gitlab.com/taurus-org/taurus/-/issues/1278). M.Navarro explains why it is not easy to reproduce.

### Taurus Recent Issues - Round Table
- E.Morales talks about the latest issues created in Taurus:
  - [#1307](https://gitlab.com/taurus-org/taurus/-/issues/1307): Pending a review of the two proposed options from N.Leclercq and Z.Reszela.
  - [#1308](https://gitlab.com/taurus-org/taurus/-/issues/1307) and [#1317](https://gitlab.com/taurus-org/taurus/-/issues/1377): M.Gandor has created two scripts that perform conversion between Taurus 4 and Taurus 5 _.pck_ files. Pending review with the intention to incorporate as a tool inside the project.
  - [#1313](https://gitlab.com/taurus-org/taurus/-/issues/1313): Good suggestion. Implementation is pending. M.Caixal provided suggestions on how it could be implemented.
  - [#1315](https://gitlab.com/taurus-org/taurus/-/issues/1315): Feature to be implemented inside Taurus_pyqtgraph. Asked MaxIV if they will implement it or not. B.Bertrand said that they will say something when they have their development planning.
  - [#1318](https://gitlab.com/taurus-org/taurus/-/issues/1318): Memory leak seems similar to one resolved not long ago. E.Morales will investigate it and propose a solution.
  - [#1316](https://gitlab.com/taurus-org/taurus/-/issues/1316): Issue related to Taurus_pyqtgraph. Pending review.

### Ongoing Developments & MR Status
- M.Caixal explains the latest developments he is working on for the Taurus_pyqtgraph project.
- E.Morales explains the latest merged work:
  - [!1272](https://gitlab.com/taurus-org/taurus/-/merge_requests/1272): PtyQtWebEngine has been added as an extra dependency for Taurus-qt.
  - [!1269](https://gitlab.com/taurus-org/taurus/-/merge_requests/1269): Nexus browser bug identified and corrected.
- M.Navarro explains Double Tango DB merge requests. [!1270](https://gitlab.com/taurus-org/taurus/-/merge_requests/1270) approach will be used.

### New Releases
- E.Morales said that there are not expected more releases at least until the bug detected at issue [#1318](https://gitlab.com/taurus-org/taurus/-/issues/1318) is solved.

### Others
- N.Leclercq will ask the Tango community about events and network aliases.
- N.Leclercq and G.Communie are left to test the delayed subscriber.
- N.Leclercq says Tango Steering Committee intends to allocate funds with the intention of assisting in the development of Taurus. The intention is to subcontract a company (S2Innovation, for instance) to carry out some task that is deemed appropriate. They would like a list with:
  - Tasks proposal.
  - Resources needed.
  - Cost estimation.
- B.Bertrand asks about using pyqtgraph and PlotPyStack being deprecated.
- N.Leclercq says MaxIV contacted them. They asked about what kind of GUI technologies they are using. N.Leclercq informs about their developments in Java and about moving to Taurus. B.Bertrand explained that MaxIV seems to be facing problems with the transition from Taurus4 to Taurus5.
- E.Morales suggest next meeting on 14.02.2024.
