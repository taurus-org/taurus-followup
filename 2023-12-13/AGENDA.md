# Taurus Follow up meeting

To be held on 13/12/2023 at 15:00

## Agenda

1. Taurus Performance Optimization.
2. Taurus recent issues - round table.
3. Taurus ongoing developments & merge request status.
4. New releases.
5. Others.