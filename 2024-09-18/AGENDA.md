# Taurus Follow up meeting

Next meeting: 18.09.2024

## Agenda

1. Taurus Performance Optimization.
   - Update on TPO status and recent MRs
   - Next step: Attributes with events
2. Recent issues - round table.
3. Merge request status - ongoing developments
   - Qt6 Compatibility: Ready. Reviewers needed. https://gitlab.com/taurus-org/taurus/-/merge_requests/1300
   - TaurusLabel, TaurusLed, TaurusLCD valueChangedSignal emission: Ready. Already reviewed but I added the fix to TaurusLCD in the meantime. https://gitlab.com/taurus-org/taurus/-/merge_requests/1279
4. Taurus Oct24 milestone and Release.
   - Taurus manual tests
5. AOB
