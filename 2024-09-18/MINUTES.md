## Meeting details

Held on 18/09/2024 at 15:00 by Zoom.

## Participants

- ESO
    - Arturo Hoffstad
    - Marzena Kaczmarczyk

- SOLEIL
    - Patrick Madela

- SOLARIS
    - Maciej Mleczko

- ALBA
    - Miquel Navarro
    - Emilio Morales
    - Oriol Vallcorba

## Minutes

### Taurus Performance Optimization
- MN and OV presented the work done since the last follow-up in July. This includes several changes consolidated in a single MR (!1306), which is currently under review but has already been tested for performance. The results show that the current version of Taurus is 1.9x faster (all attributes without events) and 1.3x faster (all attributes with events) compared to Taurus 5.1.4 (pre-TPO). Tests on real GUIs from the ALBA control room show a startup time reduction of up to 40%.
- The next steps in TPO focus on improving attributes with Events. There will be a request to Tango proposing changes in subscribe_event(). An issue in Taurus (#1360) has been created for discussion.

### Recent Issues - Round Table
- EM reviewed the latest issues created since last meeting.
  - #1252 taurus not compatible with numpy 2.0.0
  - #1254 Manual panel raises an error when loading perspective
  - #1355 IssueTaurus Designer doesn't show any message when can't load taurus widgets cause of missing dependencies
  - #1357 Manually activating polling raises exception on read()
  - #1358 cache=True should not expire
  - #1359 Remove python3-future dependency from tests

### MR status and ongoing developments
- MN started by discussing some pending MR in Taurus:
  - !1300 Qt6 Compatibility: Ready. AH explained the status of this important MR, now ready for review. Additional information is available in issue #1299.
  - !1279  TaurusLabel.valueChangedSignal not emitted. Ready again after some changes and tests added.
  - !1293  Draft: Fix numpy 2.0.0 compatibility. Draft: Benjamin working on it

- MN continued reviewing the several MR merged since last meeting.
  - PyQtgraph documentation (!124 and !125)
  - !1307  fix waitForEvent when parameter 'equal' is False.
  - !1303  Add change style example in documentation.
  - !1302  Fix timestamp on read error
  - !1299 Issue 1339: change pickle.HIGHEST_PROTOCOL to pickle.DEFAULT_PROTOCOL
  - !1298  Fix TaurusPlot documentation example
  - !1297  Correct h5 dependency
  - !1296  Fix error when editing gui with taurus newgui
  - !1291  Smart save settings issue
  - !1287  Update color names to match Web colors.
  - !1285  fix undefined w variable in from TaurusNexusBrowser
  - !1289  Add read for events without subscription

### Taurus Oct24 milestone and 5.2 Release
- OV presented a proposal of pending MRs with reasonable scope for inclusion in the next release, gathered under the Oct24 milestone. Most MRs already have assigned reviewers, but some still need volunteers.
- A release calendar was proposed:
  - From 18-Sep to 29-Sep: Review pending MRs (Oct24 milestone)
  - 30-Sep: Create Release branch and MR to stable.
  - From 30Sep to 13-Oct: Manual testing. Volunteers from different institutions will test on various platforms:
    - ALBA will test on Debian 10, 12 and Conda.
    - ESO will test on Fedora 38 and 40.
    - Solaris will try to test on CentOS or Alma 9.
    - Soleil uses Rocky with conda. No testing from their side.
  - 14-Oct: Release Taurus 5.2.0
- All attendees agreed on the proposed timeline.
- AH asked about including Qt6 compatibility in the release. ALBA suggested that this large MR would require more extensive testing and reviewing, which could delay the release schedule. It was agreed to prioritize Qt6 after the release, merge it as soon as possible to avoid divergence from the develop branch, and include it in the following release, that could be during Q1-2025.

### AOB
- OV mentioned that TPO will be presented in the "Controls and Acquisition GUI Strategies" satellite meeting at the NOBUGS 2024 conference next week.
- Next meeting is scheduled for **16.10.2024**.