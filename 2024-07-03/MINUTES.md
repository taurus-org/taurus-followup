## Meeting details

Held on 03/07/2024 at 15:00 by Zoom.

## Participants

- ESO
    - Arturo Hoffstad
    - Marzena Kaczmarczyk

- SOLEIL
    - Ludmila Klenov

- ALBA
    - Jose Ramos
    - Miquel Navarro
    - Oriol Vallcorba
    - Emilio Morales

## Minutes

### Taurus Performance Optimization
- M.Navarro explains the work done during last month and the advances in the project. We got a time improvement of 60%. More details in the meeting presentation.

### Taurus Recent Issues - Round Table
- E.Morales talks about the latest issues created.
    - Regarding taurus:
        - #1350: Problems with TaurusNexusBrowser.
        - #1351: Update widgets size is giving problems.
        - A.Hoffstad explains a issue they found at ESO with default TaurusModelChooser widget. They will create a ticket and propose a solution in a new MR.
    - Regarding taurus-pyqtgraph:
        - No recent issues.

### Ongoing Developments & MR Status
- M.Navarro explains about taurus:
    - !1285: fix undefined w variable in from TaurusNexusBrowser. To be reviewed.
    - !1248: Do not read attribute in ATTR_CONF_EVENT cb. In review.
    - !1279: Draft: TaurusLabel.valueChangedSignal not emitted. Draft.
    - !1286: TPO: avoid multiple fqdn_no_alias. To be reviewed.
    - !1283: TPO: avoid multiple getElementAlias. To be reviewed.
    - !1287/!1252: Update colors names to match Web Colors standard. Merged.
- J.Ramos explains about taurus-pyqtgraph:
    - !123: Add TaurusAttribute NaN value if the attribute is not available. To be finished and reviewed.
    - !122: Save dynamic range and log mode as property configs. To be reviewed.

### New releases
- E.Morales explains that we need to finalize the testing guide for Taurus in order to start testing with community help.
- E.Morales explains that the new version of taurus-pyqtgraph 0.9.2 is available, correcting some bugs described in the CHANGELOG file.
### Others
- E.Morales introduces O.Vallcorba to the community.
- A.Hoffstad explains an issue he found during the PyQt6/PySide2 migration. He is experiencing problems with the CI Docker. J.Ramos will help him find a solution.
- A.Hoffstad explains that he needs to port some code from the old version of PyQt5 to PyQt6/PySide2 in order to maintain compatibility with Debian Stretch. We explained that the next version of Taurus will be the last to support Debian 9.
- Next meeting will be held on **18.09.2024**.