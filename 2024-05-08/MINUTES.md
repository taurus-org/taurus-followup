## Meeting details

Held on 08/05/2024 at 15:00 by Zoom.

## Participants

- ESO
    - Arturo Hoffstad

- SOLARIS
    - Wojciech Wantuch

- ALBA
    - Jose Ramos
    - Miquel Navarro
    - Emilio Morales

## Minutes

### Taurus Performance Optimization
- M.Navarro explains that he started to work in the project. Explains he made some updates to development environment. He is designing the solutions to improve the performance. First changes will be related to FQDN methods redundant calls.

### Taurus Recent Issues - Round Table
- E.Morales talks about the latest issues created.
    - #1337: Work in progress by E.Morales.
    - #1338: A.Hoffstad is working on it. We are experiencing problems with tests.
    - #1340: M.Navarro explains about a bug in Taurus polling.
    - #1341: We discuss about integrate a matplotlib plug-in inside Taurus.

### Ongoing Developments & MR Status
- M.Navarro explains about taurus:
    - !1278: Fix MouseWheel event exceptions merged.
    - !1279: Related to issue #1338. Problems with tests.
- J.Ramos explains about taurus_pyqtgraph:
    - !110: E.Morales is trying to find a solution.
    - !115: LogMode already merged.
    - !116: pyhdbpp as optional dependency merged.

### New Releases
- J.Ramos explains:
    - taurus:
        - We are working on testing guide and will be published soon. We will inform community.
    - taurus_pyqtgraph:
        - New release 0.9.0 available on PyPi. We still more people to check for library stability.

### Others

- E.Morales explains about Taurus Workshop poll results, material that will be prepared and expected people that will assist to the workshop.
- A.Hoffstad explains about a problem he is facing with synchronous command calls inside taurus GUI using TaurusCommandButton. This is causing GUI freeze until launched subtask ends (freeze main QThread). He talk about a way to implement some asynchronous calls that avoid this problems.
- Next meeting will be held on **05.06.2024**. 