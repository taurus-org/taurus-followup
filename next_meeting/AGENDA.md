# Taurus Follow up meeting

Meeting on 05.02.2025 at 15h

## Agenda

1. Round table (Highlights, issues, comments from different institutes).
2. Pending points from last meeting.
3. Taurus 5.2.1 patch release.
4. Review of recent issues.
5. Review of merge request status.
6. Next Releases.
7. AOB.
