## Meeting details

Held on 13/03/2024 at 15:00 by Zoom.

## Participants
- MAXIV
    - Antonio Bartalesi
    - Benjamin Bertrand
- ESO
    - Arturo Hoffstad
    - Marzena Kaczmarczyk
- IPANEMA
    - Marouane BEN JELLOUL
- HI JENA
    - Dominik Hollatz
- ALBA
    - Jose Ramos
    - Miquel Navarro
    - Emilio Morales

## Minutes

### Taurus Performance Optimization
- M.Navarro explains specific tasks to be done at ALBA regarding TPO: fix bugs, improve DelayedSubscriber and optionally ask for improvements on Tango.
- A.Hoffstad ask about DelayedSubscriber implementation, specifically about attribute initial value.

### Taurus Recent Issues - Round Table
- E.Morales talks about the latest issues created in Taurus.

### Ongoing Developments & MR Status
- M.Navarro explains two new MR (one fixing a memory leak that was blocking the new release).
- M.Navarro explains the current progress of two existing MR that are in review.
- M.Navarro asks the community to review !1248 as it's prefered to be reviewed by someone external to ALBA.

### New Releases
- E.Morales mentions that this is still blocked.

### Others
- We introduce some new people in the meeting.
- M.BEN JELLOUL ask about tausu debina packaging and about camera widgets refresh rate.
