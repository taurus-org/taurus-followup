# Taurus Follow up meeting

Meeting on 11.12.2024 at 15h

## Agenda

1. Taurus 5.2.1 patch release.
2. Merge request status - ongoing developments
3. Recent issues - round table.
4. Taurus Colors not following ATK colors
5. Next Releases
6. AOB
