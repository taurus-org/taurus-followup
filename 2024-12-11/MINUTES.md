## Meeting details

Held on December 11th, 2024 at 15h via Zoom.

### Participants

- Benjamin Bertrand (MAX-IV)
- Raphaël Girardot (SOLEIL)
- Arturo Hoffstad (ESO)
- Marzena Kaczmarczyk (ESO)
- Miquel Navarro (ALBA)
- Jose Ramos (ALBA)
- Emilio Morales (ALBA)
- Oriol Vallcorba (ALBA)

## Minutes

### Taurus 5.2.1 patch release

The plan for the Taurus 5.2.1 patch release is shown, with the goal of releasing it before the Christmas break. Two critical bugfixes have been already addressed ([!1334](https://gitlab.com/taurus-org/taurus/-/merge_requests/1334), [!1330](https://gitlab.com/taurus-org/taurus/-/merge_requests/1330)). A new issue raised by Benjamin regarding the initialization of a model using eval ([#1390](https://gitlab.com/taurus-org/taurus/-/issues/1390)) is blocking the upgrade at MAX-IV and needs to be fixed for the patch release. ALBA will work on it. Arturo also noted a past, similar issue involving lazy-loading when eval includes other attributes (sorry, is there an issue for it? I could not find it).

### Merge request status - ongoing developments

Open Merge Requests were reviewed for inclusion in the patch release. Smaller MRs will be included if reviewed before next week, prioritized by readiness:
- [!1319](https://gitlab.com/taurus-org/taurus/-/merge_requests/1319) Close TaurusApplication on ctrl+c (SIGINT signal)
- [!1327](https://gitlab.com/taurus-org/taurus/-/merge_requests/1327) jdraw: Fixed int for QColor args 
- [!1328](https://gitlab.com/taurus-org/taurus/-/merge_requests/1328) taurusgraphic: Close thread in the end -
- [!1331](https://gitlab.com/taurus-org/taurus/-/merge_requests/1331) Resolve "DevEnum support" -
- [!1323](https://gitlab.com/taurus-org/taurus/-/merge_requests/1323) Prevent TaurusValue to return a non-list result as default read widget and to destroy an unexisting widget

The MR [!1322](https://gitlab.com/taurus-org/taurus/-/merge_requests/1323) "Use TaurusModelSelector in TaurusForm (2)" which was dropped from the 5.2 release during testing will not be included yet in the release, as it is not needed by ESO, and it will be reviewed afterwards.

### Recent Issues - Round Table

In addition to the eval issue previously discussed, Raphaël highlighted two new issues concerning taurus_pyqtgraph. These issues will be transferred from the Taurus repository to the taurus_pyqtgraph repository for resolution.

### Taurus Colors not following ATK colors

Raphaël reported a mismatch between Taurus colors and Tango ATK colors, specifically for the Running state. In Taurus, the color is blue, while in ATK, it is dark green. Additionally, there was a proposal to change certain foreground (text) colors to white for better readability on dark backgrounds. While there was general agreement to implement these small changes, the discussion broadened to include the implementation of configurable color schemas to address also (old and recurrent) requests to adapt colors for color-blind users. ESO and ALBA expressed interest on this feature, as several users are affected by color-blindness. Some custom solutions currently in use were shared, for example, for the led widget, where ALBA adds a label next to the led and ESA uses svg files dynamically modifying their colors to render them with the desired color (and caching for reuse). It was agreed to put solutions in common for potential integration into Taurus.

### Next Releases

For the next minor release (5.3), Qt6 support will be added. The choice of the default library (PyQt6 vs. PySide6) was discussed, with the following considerations:
- Licencing implications.
- Not all current Taurus functionality (e.g., custom widgets and Designer integration) is available for PySide6.
- No existing conda package for PyQt6 at this time.

Arturo will rebase the MR after the patch release so it can be reviewed for 5.3 release.

### AOB

- Arturo shared details about a recent ESO consortia meeting involving multiple institutions and instruments where a workshop on Taurus was carried out. Taurus received positive feedback, with users particularly appreciating the designer tool and the ability to create custom widgets through graphical solutions. Arturo suggested improving the widget gallery in the Taurus documentation to better showcase available widgets and useful classes (e.g. ProtectedTaurusMessageBox)

- Marzena mentioned that she is developing the third taurus plugin for ESO.

- Next meeting is scheduled for **February 2nd, 2025**.