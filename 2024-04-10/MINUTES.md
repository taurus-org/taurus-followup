## Meeting details

Held on 10/04/2024 at 15:00 by Zoom.

## Participants
- MAXIV
    - Antonio Bartalesi
    - Benjamin Bertrand
- ESO
    - Arturo Hoffstad
    - Marzena Kaczmarczyk
- SOLEIL
    - Ludmila Klenov
- HI JENA
    - Dominik Hollatz
- ALBA
    - Jose Ramos
    - Miquel Navarro
    - Emilio Morales

## Minutes

### Taurus Performance Optimization
- M.Navarro explains that it's ready to start, it's just delaying a bit for checking the existing MRs.

### Taurus Recent Issues - Round Table
- E.Morales talks about the latest issues created in Taurus (see [presentation slide 5 and 6](presentations/taurus_followup_10042024.pdf)).

### Ongoing Developments & MR Status
- M.Navarro explains:
    - _Multiple Tango Host_ MR is ready to be merged.
    - _Tango attribute property "display_unit"_ MR is harder to implement than we initially though. We propose a way to implement it that could be easier.
    - _Deprecated MouseWheelEvent_ MR will be recreated to include the changes in two widgets and simplify the fix (using the fix they made in pyqtgraph)
    - _Taurus Trend in taurusdesigner_ is being checked by E.Morales.
- J.Ramos explains that we already merged the _x axis time range selector_ and _raw text data export_ MRs.

### New Releases
- E.Morales mentions that the release is not blocked anymore by MR, but we still need to create the testing guide for a stable release.
- J.Ramos mentions that a new taurus_pyqtgraph will be released soon with the new changes.

### Others

- E.Morales explains that there will be a Taurus Workshop in the Tango Meeting. We propose to divide it in 3 parts: introduction to Taurus, tutorial and open discussion. He explains that we will ask who will come to the workshop so we can send them a poll to ask for their interests. Then we will adapt the tutorial to fit their needs.

- A.Hoffsta explains his progress in PyQt6 support. His notes:
```
# Taurus support for Qt6

All unit test passing, but the execution time is really long.
Need further investigation.

Python changes
* threading.Thread.setDaemon deprecated in favor of daemon property.
** This change was introduced before in python 3.5.
* numpy.bool8 replaced by numpy.bool_.
** At least introduced in numpy 1.13. Could not find earlier documentation to see when this change was introduced.

This raises the question:
* What are our supported python versions.
** I see that developer guide mentions 3.5+ https://www.taurus-scada.org/devel/coding_guide.html
** A: debian 9 -> check numpy version available. (check with sardana devs)
** CI indicates 3.6+

Qt changes (which affects both PyQt6 and PySide6):
* Enums without complete namespace added back dynamically.
* qApp no longer provided.
** We could reintroduce it, and mark as deprecated.
* QAction constructor signature reordered.
** QAction are created without arguments, and then setters are used.
** KeySequences were replaced by their string representation.

Notes:
** PyQt6 (need to check supported python versions, could not find info. At least 3.11+)
** PySide6 only support Python 3.12+
** For the future: I think we should consider replacing taurus.external.qt for QtPy.
```