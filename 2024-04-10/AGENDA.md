# Taurus Follow up meeting

## Agenda

1. Taurus Performance Optimization.
2. Taurus recent issues - round table.
3. Taurus ongoing developments & merge request status.
4. New releases.
5. Others.